1. inp (input card submit)
must include (1) inp card, (2) phits.in card, and (3) job submission script for parallel, multi-core job 

2. output will include the following files:
- batch.now
- data output *name given under TCross card. There will be n files where n is equal to the number of batches (cpus requested) in the input card under maxbc = n. These files will be labeled *name_dmp_dat.00n.
- the *.out file specified in the file(6) = *name.out portion of the input card

3. you can combine all of the *name_dmp_dat.00n. files into a single file by using the linux command "cat" (concatenate and print files). More info about the command is found by typing "man cat" from the terminal. To merge multiple files are name output_dmp_dat_001, output_dmp_dat_002, etc... you can type the following:

cat output_dmp_dat_* >> output.dat

**this essentially combines all the output text files into one single filem, which can be quite large if you have a large run.

4. the *name_dmp_dat.00n files will have the requested data, but in a format that C,C++ cannot interpret, using "D" instead of "e" for declaring exponential, e.g. 1.076255954997692D+03. You can change this using the "sed" command (stream editor). Again, more info can be found with "man sed". 

assuming your file is called output.dat the command:

sed -in 's/D/e/gpw output.out' output.dat

creates a new file labeled "output.out" with the "D's" changed to "e"

5. use LETBlock_Parse.c to translate PHITS k code and energies into output usable by ROOT
