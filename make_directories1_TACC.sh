#!/bin/bash

#shell script to make the directories -- added the hyphen to the directory names

material=( mat1 mat2 mat3 mat4 mat5 mat6 mat7 mat8 mat9 mat10 )
depth=( 5cm 10cm 15cm 20cm 25cm 30cm 35cm 40cm 45cm 50cm 55cm 60cm 65cm 70cm 75cm 80cm 85cm 90cm 95cm 100cm )

for i in "${material[@]}"
do
    mkdir $i
    cd $i
    for j in "${depth[@]}"
    do
          mkdir $j-$i
    done
    cd ..
done

exit 0
