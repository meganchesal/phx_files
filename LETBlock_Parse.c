// reads in a file specificed by ifp
// converts input from PHITS into long floating point intergers
// removes exponent and trailing zeros to convert output into readable PHITS kf code
// writes output into file analysis.txt

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main()
{
	FILE *ifp, *ofp, *pcofp;
	char *mode = "r";
	char file_name[50];
	char outputFilename[50];
	char pcoutputFilename[50];
	double charge,charge2;
	double AtomicMass;
	double FB,g,g2,Beta,Beta2,LET,xpos,ypos,xdir,ydir,zdir,collision_number,history_number,batch_number,cascade_id;
	double rmn = 939.57, rmp = 938.3;
	double etot,etot2,mc2,pc;
	double reMuon = 105.66, Pion = 139.60;
	double perme; //proton energy + nuclei rest mass energy
	double nerme; //neutron energy + nuclei rest mass energy
	char ion[30];
	float energy,re,k;
	float lnI = 4.312;  //I=mean excitation energy of H2O
	int i=0,j=0,cc,status,z,zz,A,pion=0,muon=0;
	int HZE[26];

    // printf("Enter the name of file.xxx ");
    // gets(file_name);
    // printf("Enter the name of the output file.xxx ");
    // gets(outputFilename);
    // printf("Enter the name of the particle count output file.xxx ");
    // gets(pcoutputFilename);

	for (i=0; i<27; i++){
		HZE[i] = 0;
	}

//***************************************************************************
//	open the file and parse                                                \\
//***************************************************************************

	// ifp = fopen(file_name, mode);
	ifp = fopen("output.out", mode);
	if (ifp == NULL) {
	  fprintf(stderr, "Can't open input file in.list!\n");
	  	  exit(1);
	}

	// ofp = fopen(outputFilename, "w");
	ofp = fopen("output_PARSED.dat", "w");

	if (ofp == NULL) {
	  fprintf(stderr, "Can't open output file %s!\n",
	          outputFilename);
			  	  exit(1);
	}

	while (!feof(ifp)) {
		fscanf(ifp,"%lf", &charge);
		fscanf(ifp,"%f", &energy);
		fscanf(ifp,"%lf", &xpos);
		fscanf(ifp,"%lf", &ypos);
		fscanf(ifp,"%lf", &xdir);
		fscanf(ifp,"%lf", &ydir);
		fscanf(ifp,"%lf", &zdir);
		fscanf(ifp,"%lf", &collision_number);
		fscanf(ifp,"%lf", &history_number);
		fscanf(ifp,"%lf", &batch_number);
		fscanf(ifp,"%lf", &cascade_id);
		j++;
		AtomicMass = 0;
//***************************************************************************
//	search for pions                                                       \\
//***************************************************************************
		if (charge == 2.110000000000000e+02 || charge == -2.110000000000000e+02 ||
		charge == 1.110000000000000e+02 ) {
			//make kf code more manageable
			printf("found pion \n");
			pion++;
			z = 1;
			AtomicMass = 1;
			A = AtomicMass;
			re = 139.60;
			Beta=sqrt(1-((1/((energy/re)+1))*(1/((energy/re)+1))));
			Beta2 = Beta*Beta;
			k = (5.08e-31*3.34e29*z*z)/Beta2;
			FB=log((Beta2/(1-Beta2)))-Beta2;
			LET = k*(13.8373+FB-lnI);  //LET in keV/micron no shell or density effect corrections
			//LET in keV/micron no shell or density effect corrections
		}
//***************************************************************************
//	search for muons                                                       \\
//***************************************************************************
		else if (charge == 1.30000000000000e+02 || charge == -1.30000000000000e+02 ) {
			//make kf code more manageable
			printf("found muon \n");
			muon++;
			z = 1;					   // all muons
			AtomicMass = 1;
			A = AtomicMass;
			perme = z*(energy+rmp);
			nerme = (AtomicMass-z)*(energy+rmn);
			etot = perme+nerme;
			etot2 = etot*etot;
			mc2 = ((z*rmp)+((AtomicMass-z)*rmn))*((z*rmp)+((AtomicMass-z)*rmn));
			pc = sqrt(etot2-mc2);
			Beta = pc/etot;
			Beta2 = Beta*Beta;
			zz = z*z;
			LET = 0.1*(((5.08E-31)*(zz)*(3.34E+29))/(Beta2))*(log((1020000)*(Beta2)/(74.6*(1-(Beta2))))-(Beta2));
			//LET in keV/micron no shell or density effect corrections
		}
//***************************************************************************
//	search for neutrino **LET calculations not implemented                 \\
//***************************************************************************
		else if (charge == 1.200000000000000e+01 || charge == -1.200000000000000e+01 || charge == 1.400000000000000e+01 || charge == -1.400000000000000e+01) { //make kf code more manageable
			printf("found neutrino \n");
			z = 0;					   // neutrino
			A = z;
			re = 0;
			Beta=0;
			Beta2 = Beta*Beta;
			k = 0;
			FB=0;
			LET = 0;  //LET in keV/micron no shell or density effect corrections
		}
//***************************************************************************
//	search for protons                                                     \\
//***************************************************************************
		else if (charge == 2.212000000000000e+03) { //make kf code more manageable
			printf("found proton \n");
			z = 1;
			HZE[1]++;
			AtomicMass = 2;
			A = AtomicMass;
			perme = z*(energy+rmp);
			nerme = (AtomicMass-z)*(energy+rmn);
			etot = perme+nerme;
			etot2 = etot*etot;
			mc2 = ((z*rmp)+((AtomicMass-z)*rmn))*((z*rmp)+((AtomicMass-z)*rmn));
			pc = sqrt(etot2-mc2);
			Beta = pc/etot;
			Beta2 = Beta*Beta;
			zz = z*z;
			LET = 0.1*(((5.08E-31)*(zz)*(3.34E+29))/(Beta2))*(log((1020000)*(Beta2)/(74.6*(1-(Beta2))))-(Beta2));
			//LET in keV/micron no shell or density effect corrections
		}
//***************************************************************************
//	search for neutrons **not implemented in LET calculations              \\
//***************************************************************************
		else if (charge == 2.112000000000000e+03) {
			printf("found neutron \n");
			z = 0;
			HZE[0]++;					   // recode neutron
			AtomicMass = 0;
			A = AtomicMass;
			re = 0;
			Beta=0;
			Beta2 = Beta*Beta;
			k = 0;
			FB=0;
			LET = 0;  //LET in keV/micron no shell or density effect corrections
		}
//***************************************************************************
//	search for HZE particles                                               \\
//***************************************************************************
		else if (charge > 3000) {					// recode HZE
			printf("found HZE \n");
			charge2 = charge;
			z = charge/1000000;
			HZE[z]++;
			charge = z*1000000.;
			AtomicMass = charge2 - charge;
			A = AtomicMass;
			perme = z*(energy+rmp);
			nerme = (AtomicMass-z)*(energy+rmn);
			etot = perme+nerme;
			etot2 = etot*etot;
			mc2 = ((z*rmp)+((AtomicMass-z)*rmn))*((z*rmp)+((AtomicMass-z)*rmn));
			pc = sqrt(etot2-mc2);
			Beta = pc/etot;
			Beta2 = Beta*Beta;
			zz = z*z;
			LET = 0.1*(((5.08E-31)*(zz)*(3.34E+29))/(Beta2))*(log((1020000)*(Beta2)/(74.6*(1-(Beta2))))-(Beta2)); //LET in keV/micron no shell or density effect corrections
		}
		else  {
			charge = 0.;
			z = charge;
			AtomicMass = 0;
			A = 0;
			re = 0;
			Beta = 0;
			Beta2 = 0;
			k = 0;
			FB = 0;
			LET = 0;
		}
		if(z > 0){
			printf("z = %u\n", z);
			printf("A = %f\n", AtomicMass);
			printf("LET = %f\n",LET);
			printf("\n");
			fprintf(ofp, "%u     ",z);
			fprintf(ofp, "%u     ",A);
			fprintf(ofp, "%e     ",energy);
			fprintf(ofp, "%f     ",LET);
			fprintf(ofp, "%f     ",xpos);
			fprintf(ofp, "%f     ",ypos);
			fprintf(ofp, "%f     ",xdir);
			fprintf(ofp, "%f     ",ydir);
			fprintf(ofp, "%f     ",zdir);
			fprintf(ofp, "%f     ",collision_number);
			fprintf(ofp, "%f     ",history_number);
			fprintf(ofp, "%f     ",batch_number);
			fprintf(ofp, "%f     ",cascade_id);
			fprintf(ofp,"\n");
		}
	}

	// pcofp = fopen(pcoutputFilename, "w");
	pcofp = fopen("output_pc.out", "w");
	fprintf(pcofp,"\n");
	fprintf(pcofp,"\n");
	fprintf(pcofp," particle count  ");
	fprintf(pcofp,"_________________\n");
	fprintf(pcofp,"\n");
	fprintf(pcofp,"pion         %u\n",pion);
	fprintf(pcofp,"muon         %u\n",muon);
	for (i=0; i<27; i++) {
		fprintf(pcofp, "%u            ",i);
		fprintf(pcofp, "%u ",HZE[i]);
		fprintf(pcofp,"\n");
	}
 return 0;
}
