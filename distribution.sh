#!/bin/bash

depth=( 5cm 10cm 15cm 20cm 25cm 30cm 35cm 40cm 45cm 50cm 55cm 60cm 65cm 70cm 75cm 80cm 85cm 90cm 95cm 100cm )
material=( GYP )

#mkdir 1GeV_Fe26_GYP
#cd 1GeV_Fe26_GYP  -> moved these inside the for loop

for j in "${material[@]}"
do
	mkdir 1GeV_Fe26_$j
	cd 1GeV_Fe26_$j 
	for i in "${depth[@]}"
	do
		mkdir $i-$j
		cp /work/mchesal/1GeV_Fe26_PE/$i-PE/*PE.inp $i-$j
		cp /work/mchesal/1GeV_Fe26_PE/$i-PE/*PE_mpi.pbs $i-$j
	done
done

exit 0  

