How to submit phits batch jobs on the LSU cluster -- including installing of phits

1. Everything goes through the home directory
    - on the LSU HPC, everything is done through the home directory, no matter where your installation is.  The home directory allows for permanent data storage, but only has 5GB of space.
    - The HPC recommends that all programs and executables be in the home directory, as the work directory can be subject to purges, however phits does not fit on the home directory (disk quota exceeded).
    - Even though phits needs to be installed in the work directory, all the executables look for files on the home directory

IMPORTANT:  on first login, the work directory does not exist.  It will be created in ~1hr, so login later and the work directory will be there
home directory --> /home/username
work directory --> /work/username

2. Install phits
    - copy the phits.zip file onto the LSU HPC and unzip in the work directory (as mentioned before, home directory does not hold enough space) -- phits.zip file should be in the linux install folder
    login$ scp ~/phits.zip username@smic.hpc.lsu.edu:/work/username/
    login$ unzip phits.zip
    - this will unzip the file and download all of the files and data.  Could take some outputFilename

3. Adjust and run the makefile
    - LSUHPC has the option to use various compiler (Intel, GNU, etc.); The intel compiler didn't work for me, but the GNU compiler for MPI codes work fine (intel for C codes (icc) still works)
    - go to the source directory in phits --> /work/username/phits/src/
    - open the makefile --> vim makefile (or use another text editor that works in the terminal, like nano)
    - For running phits in parallel, we use MPI, make sure to uncomment 'USEMPI = true' near the top or else it will not work; also that 'ENVFLAGS = LinIfort' because this is a linus system
    - Under the section for 'Linux Intel Fortran', change the first compiler in the subsection 'ifeq($(USEMPI),true)' to 'mpif90' -->  'FC = mpif90'
    - save and exit the file
    - while still in this directory, run the make file --> login$ make
    - this step will take quite awhile

4. Change and adjust the PHITS input file
    - because everything on the LSUHPC works through the home directory, the executable (phits_LinIfort_MPI) will still automatically look for the phits.in file in the home directory, even if the batch is executed in the work directory
    - unless you want all of the files to show up in the home directory where the phits.in file is, need to change some values in the phits input file
    - changes:
          file(6) = /work/username/output-directory/file_summary.out  # file name and location of the output summary
          file(7) = /work/username/phits/data/xsdir.jnd               # file name and location of nuclear data input
          file(22)= /work/username/output-direcotyr/batch.out         # file name of the batch out file and its location
          under [ T - C R O S S ] -->
          file = /work/username/output-directory/filename.dat         # name of the data files that will come out
    - these will put the output files from phits into the work directory rather than where the phits.in file is
    - however, the batch and error files will still show up in the same directory where you submitted the batch file 

5. The batch script file
    - LSUHPC uses qsub to submit batch files, put the submit file in that kind of format.  The file does not need to be in any particular data format, I've been labeling them 'filename_mpi.pbs'
    - The basic format of the file is as follows (with no additional data changes):

    #!/bin/bash
    #PBS -N test5cm                         # Job Name
    #PBS -1 nodes=8:ppn=20                  # Number of nodes and processors per node (ppn max is 20)
    #PBS -1 walltime=00:15:00               # Maximum wall time
    #PBS -q checkpt                         # Queue Name
    #PBS -o batch                           # File name for standard output
    #PBS -e errors                          # File name for standard error
    #PBS -A hpc_gcrblock01                  # Allocation Name, if only one allocation, not needed
    #PBS -m abe                             # Send mail when job begins and ends and (if it happens) aborts
    #PBS -M mchesa1@lsu.edu                 # Send mail to this address

    module list
    pwd
    date

    mpirun -machinefile $PBS_NODEFILE -np 160 /work/mchesal/phits/phits_LinIfort_MPI

    - some notes:
        = several kinds of queues: single (max walltime = 72 hrs | max nodes per job = 1), workq (max walltime = 72 hrs | max nodes per job = 128 | jobs are not preemptable), checkpt (max walltime = 72 hrs | max nodes per job = 200 | jobs are preemptable)
        = Allocation name may or may not be needed, but better to have it just in case
        = in mpirun statement '-machinefile $PBS_NODEFILE' are required statements
        = '-np 160' will dictate the number of output files you will get --> will have one less than the value after -np.  Get this number by multiplying nodes and ppn (i.e. 8*20 = 160)
        = comments are not allowed on the actual submit file, it gets confused; make sure they are deleted when you submit

6. Submitting the batch file
    - wherever you want the LSUHPC standard output and errors to end up, should submit the batch file from that directory (could possibly change that in the submission file)
    - but all batch submissions should take place in the work directory (don't submit from the home directory, its not meant for that)
    - submit with login$ qsub filename_mpi.pbs
    - can monitor the status of the job with login$ qstat

7. If the code didn't work, errors and couldn't find files
    - check .bash_profile that the PATH was correctly entered
    - Under "export PATH" should have
	export PHITSPATH=/work/username/phits
	export PATH=/work/username/phits/bin:${PATH}
	export PATH=/work/username/phits/dchain-sp/bin:${PATH}
