%reading out the data into a useable form (stick it in a matrix) 

fileID = fopen("deposit0-final.out", 'r+');
A = fscanf(fileID, '%f'); 

im3 = [512, 512];
%A = [1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16];
%im = zeros(4,4);

counts3 = 1; 

for i = 1:1:512
    for j = 1:1:512
        im3(i,j) = A(counts3);
        counts3 = counts3 + 1; 
    end
end
 

figure(); a = gca;
c = imagesc(flipud(im3)); set(gca, 'YDir', 'normal');
a.TickLabelInterpreter = 'latex'; 
colormap('jet'), colorbar;
%Sx = sym(-30:5:30); a.XTick = double(Sx);
%a.XTickLabel = strcat('$',arrayfun(@latex,Sx,'UniformOutput',false), '$');
%Sy = sym(-45:5:45); a.YTick = double(Sy);
%a.YTickLabel = strcat('$',arrayfun(@latex,Sy,'UniformOutput',false), '$');
xlabel('X position', 'FontSize', 14, 'Interpreter', 'latex');
ylabel('Y position', 'FontSize', 14, 'Interpreter', 'latex');
title("Dose Deposition in Human Phantom" + newline + ...
    "with i=1:1:512 and j=1:1:512", 'FontSize', 18, 'Interpreter', 'latex');

