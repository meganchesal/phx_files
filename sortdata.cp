#include <fstream>
#include <iostream>
#include <string>

using namespace std;

int main() {

	cout << "writing deposit files" << endl;

	ifstream filein("deposit-xy-MeVcm3.out");
	if (!filein) {
		cout << "Error opening files!" << endl;
		return 1;
	}

	string line;
	string search = "newpage:";
	string esearch = "gshow";
	int curline = 0;
	int index = 0; int index2 = 0;
	int dpos[30]; int epos[30];

	while (getline(filein, line)) {
		curline++;
		if (line.find(search, 0) != string::npos) {
			cout << "found: " << search << " | " << "line : " << curline << endl;
			dpos[index] = curline;
			index++;
		}
		if (line.find(esearch, 0) != string::npos) {
			cout << "found: " << esearch << " | " << "line: " << curline << endl;
			if (curline > 256) {
				epos[index2] = curline;
				index2++;
			}
		}
	}

	filein.close();
	ofstream fout0("deposit0.out"); ofstream fout1("deposit1.out"); ofstream fout2("deposit2.out");
	ofstream fout3("deposit3.out"); ofstream fout4("deposit4.out"); ofstream fout5("deposit5.out");
	ofstream fout6("deposit6.out"); ofstream fout7("deposit7.out"); ofstream fout8("deposit8.out");
	ofstream fout9("deposit9.out"); ofstream fout10("deposit10.out"); ofstream fout11("deposit11.out");
	ofstream fout12("deposit12.out"); ofstream fout13("deposit13.out"); ofstream fout14("deposit14.out");
	ofstream fout15("deposit15.out"); ofstream fout16("deposit16.out"); ofstream fout17("deposit17.out");
	ofstream fout18("deposit18.out"); ofstream fout19("deposit19.out"); ofstream fout20("deposit20.out");
	ofstream fout21("deposit21.out"); ofstream fout22("deposit22.out"); ofstream fout23("deposit23.out");
	ofstream fout24("deposit24.out"); ofstream fout25("deposit25.out"); ofstream fout26("deposit26.out");
	ofstream fout27("deposit27.out"); ofstream fout28("deposit28.out"); ofstream fout29("deposit29.out");
	ifstream infile("deposit-xy-MeVcm3.out");
	//sort the data into individual files
	string line2; int counts = 0;

    while (getline(infile, line2)) {
        counts++;
        //deposit0.out
        if (counts >= dpos[0] && counts < epos[0] - 1) {
            if (counts == dpos[0]) { cout << "writing deposit0.out" << endl; }
            fout0 << line2 << endl;
        }
        if (counts == epos[0]) { cout << "completed deposit0.out " << endl; }
        //deposit1.out
        if (counts >= dpos[1] && counts < epos[1] - 1) {
            if (counts == dpos[1]) { cout << "writing deposit1.out" << endl; }
            fout1 << line2 << endl;
        }
        if (counts == epos[1]) { cout << "completed deposit1.out " << endl; }
        //deposit2.out
        if (counts >= dpos[2] && counts < epos[2] - 1) {
            if (counts == dpos[2]) { cout << "writing deposit2.out" << endl; }
            fout2 << line2 << endl;
        }
        if (counts == epos[2]) { cout << "completed deposit2.out " << endl; }
        //deposit3.out
        if (counts >= dpos[3] && counts < epos[3] - 1) {
            if (counts == dpos[3]) { cout << "writing deposit3.out" << endl; }
            fout3 << line2 << endl;
        }
        if (counts == epos[3]) { cout << "completed deposit3.out " << endl; }
        //deposit4.out
        if (counts >= dpos[4] && counts < epos[4] - 1) {
            if (counts == dpos[4]) { cout << "writing deposit4.out" << endl; }
            fout4 << line2 << endl;
        }
        if (counts == epos[4]) { cout << "completed deposit4.out " << endl; }
        //deposit5.out
        if (counts >= dpos[5] && counts < epos[5] - 1) {
            if (counts == dpos[5]) { cout << "writing deposit5.out" << endl; }
            fout5 << line2 << endl;
        }
        if (counts == epos[5]) { cout << "completed deposit5.out " << endl; }
        //deposit6.out
        if (counts >= dpos[6] && counts < epos[6] - 1) {
            if (counts == dpos[6]) { cout << "writing deposit6.out" << endl; }
            fout6 << line2 << endl;
        }
        if (counts == epos[6]) { cout << "completed deposit6.out " << endl; }
        //deposit7.out
        if (counts >= dpos[7] && counts < epos[7] - 1) {
            if (counts == dpos[7]) { cout << "writing deposit7.out" << endl; }
            fout7 << line2 << endl;
        }
        if (counts == epos[7]) { cout << "completed deposit7.out " << endl; }
        //deposit8.out
        if (counts >= dpos[8] && counts < epos[8] - 1) {
            if (counts == dpos[8]) { cout << "writing deposit8.out" << endl; }
            fout8 << line2 << endl;
        }
        if (counts == epos[8]) { cout << "completed deposit8.out " << endl; }
        //deposit9.out
        if (counts >= dpos[9] && counts < epos[9] - 1) {
            if (counts == dpos[9]) { cout << "writing deposit9.out" << endl; }
            fout9 << line2 << endl;
        }
        if (counts == epos[9]) { cout << "completed deposit9.out " << endl; }
        //deposit10.out
        if (counts >= dpos[10] && counts < epos[10] - 1) {
            if (counts == dpos[10]) { cout << "writing deposit10.out" << endl; }
            fout10 << line2 << endl;
        }
        if (counts == epos[10]) { cout << "completed deposit10.out " << endl; }
        //deposit11.out
        if (counts >= dpos[11] && counts < epos[11] - 1) {
            if (counts == dpos[11]) { cout << "writing deposit11.out" << endl; }
            fout11 << line2 << endl;
        }
        if (counts == epos[11]) { cout << "completed deposit11.out " << endl; }
        //deposit12.out
        if (counts >= dpos[12] && counts < epos[12] - 1) {
            if (counts == dpos[12]) { cout << "writing deposit12.out" << endl; }
            fout12 << line2 << endl;
        }
        if (counts == epos[12]) { cout << "completed deposit12.out " << endl; }
        //deposit13.out
        if (counts >= dpos[13] && counts < epos[13] - 1) {
            if (counts == dpos[13]) { cout << "writing deposit13.out" << endl; }
            fout13 << line2 << endl;
        }
        if (counts == epos[13]) { cout << "completed deposit13.out " << endl; }
        //deposit14.out
        if (counts >= dpos[14] && counts < epos[14] - 1) {
            if (counts == dpos[14]) { cout << "writing deposit14.out" << endl; }
            fout14 << line2 << endl;
        }
        if (counts == epos[14]) { cout << "completed deposit14.out " << endl; }
        //deposit15.out
        if (counts >= dpos[15] && counts < epos[15] - 1) {
            if (counts == dpos[15]) { cout << "writing deposit15.out" << endl; }
            fout15 << line2 << endl;
        }
        if (counts == epos[15]) { cout << "completed deposit15.out " << endl; }
        //deposit16.out
        if (counts >= dpos[16] && counts < epos[16] - 1) {
            if (counts == dpos[16]) { cout << "writing deposit16.out" << endl; }
            fout16 << line2 << endl;
        }
        if (counts == epos[16]) { cout << "completed deposit16.out " << endl; }
        //deposit17.out
        if (counts >= dpos[17] && counts < epos[17] - 1) {
            if (counts == dpos[17]) { cout << "writing deposit17.out" << endl; }
            fout17 << line2 << endl;
        }
        if (counts == epos[17]) { cout << "completed deposit17.out " << endl; }
        //deposit18.out
        if (counts >= dpos[18] && counts < epos[18] - 1) {
            if (counts == dpos[18]) { cout << "writing deposit18.out" << endl; }
            fout18 << line2 << endl;
        }
        if (counts == epos[18]) { cout << "completed deposit18.out " << endl; }
        //deposit19.out
        if (counts >= dpos[19] && counts < epos[19] - 1) {
            if (counts == dpos[19]) { cout << "writing deposit19.out" << endl; }
            fout19 << line2 << endl;
        }
        if (counts == epos[19]) { cout << "completed deposit19.out " << endl; }
        //deposit20.out
        if (counts >= dpos[20] && counts < epos[20] - 1) {
            if (counts == dpos[20]) { cout << "writing deposit20.out" << endl; }
            fout20 << line2 << endl;
        }
        if (counts == epos[20]) { cout << "completed deposit20.out " << endl; }
        //deposit21.out
        if (counts >= dpos[21] && counts < epos[21] - 1) {
            if (counts == dpos[21]) { cout << "writing deposit21.out" << endl; }
            fout21 << line2 << endl;
        }
        if (counts == epos[21]) { cout << "completed deposit21.out " << endl; }
        //deposit22.out
        if (counts >= dpos[22] && counts < epos[22] - 1) {
            if (counts == dpos[22]) { cout << "writing deposit22.out" << endl; }
            fout22 << line2 << endl;
        }
        if (counts == epos[22]) { cout << "completed deposit22.out " << endl; }
        //deposit23.out
        if (counts >= dpos[23] && counts < epos[23] - 1) {
            if (counts == dpos[23]) { cout << "writing deposit23.out" << endl; }
            fout23 << line2 << endl;
        }
        if (counts == epos[23]) { cout << "completed deposit23.out " << endl; }
        //deposit24.out
        if (counts >= dpos[24] && counts < epos[24] - 1) {
            if (counts == dpos[24]) { cout << "writing deposit24.out" << endl; }
            fout24 << line2 << endl;
        }
        if (counts == epos[24]) { cout << "completed deposit24.out " << endl; }
        //deposit25.out
        if (counts >= dpos[25] && counts < epos[25] - 1) {
            if (counts == dpos[25]) { cout << "writing deposit25.out" << endl; }
            fout25 << line2 << endl;
        }
        if (counts == epos[25]) { cout << "completed deposit25.out " << endl; }
        //deposit26.out
        if (counts >= dpos[26] && counts < epos[26] - 1) {
            if (counts == dpos[26]) { cout << "writing deposit26.out" << endl; }
            fout26 << line2 << endl;
        }
        if (counts == epos[26]) { cout << "completed deposit26.out " << endl; }
        //deposit27.out
        if (counts >= dpos[27] && counts < epos[27] - 1) {
            if (counts == dpos[27]) { cout << "writing deposit27.out" << endl; }
            fout27 << line2 << endl;
        }
        if (counts == epos[27]) { cout << "completed deposit27.out " << endl; }
        //deposit28.out
        if (counts >= dpos[28] && counts < epos[28] - 1) {
            if (counts == dpos[28]) { cout << "writing deposit28.out" << endl; }
            fout28 << line2 << endl;
        }
        if (counts == epos[28]) { cout << "completed deposit28.out " << endl; }
        //deposit29.out
        if (counts >= dpos[29] && counts < epos[29] - 1) {
            if (counts == dpos[29]) { cout << "writing deposit29.out" << endl; }
            fout29 << line2 << endl;
        }
        if (counts == epos[29]) { cout << "completed deposit29.out " << endl; }


  }


  infile.close();

	return 0;
}