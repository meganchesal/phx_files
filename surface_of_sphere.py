#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 15:09:41 2020

@author: meganchesal
"""

import numpy as np
import random
import os 

def sample_spherical(npoints, ndim=3):
    vec = np.random.randn(ndim, npoints)
    vec /= np.linalg.norm(vec, axis=0)
    return vec

list = []
kf = np.array(list)
en = np.array(list)
count = 0
#name of the PHITS output file being opened for data, will need to be changed depending on the file
with open("sample_data.txt") as fp:
    for line in fp: 
        count += 1
        d = line.split()
        kf = np.append(kf, float(d[0]))
        en = np.append(en, float(d[1]))
        
N = count #number of iterations, based on the number of particles read
phi = np.linspace(0, np.pi, 200)
theta = np.linspace(0, 2 * np.pi, 400)
x = np.outer(np.sin(theta), np.cos(phi))
y = np.outer(np.sin(theta), np.sin(phi)) 
z = np.outer(np.cos(theta), np.ones_like(phi)) 

xi, yi, zi = sample_spherical(N)

#above code normalizes points, to get the sphere to be the radius you want, multiply by R, like below

R = 100

wgt = np.full(N, 3.0) #weight of the particle, to match the GCR spectrum 
#for 20cm --> wgt = 3
#for 30cm --> wgt = 25
#for 40cm --> wgt = 1000
s = np.vstack([kf,en,R*xi,R*yi,R*zi, wgt])
v = np.transpose(s)

#saved filed in a text file.  Each row represents a new particle and each column is the attribute, saved as:
# [kf particle code] [energy of particle] [xi pos] [yi pos] [zi pos] [wgt]
np.savetxt('testing.dat', v, delimiter=' ')

#will also create a new data file in the PHITS source format.  This would be used if you were including the 
#source into the input card with a "infl:{datafile.inp}" statement
#file format will be as follows: 
#1   <source> = 1                        --> normalization factor in PHITS, set as 1 for all particles
#2     s-type = 1                        --> sets source shape, this is set to cylinder shape
#3       proj = [kf particle code]       --> particle type, specified by the kf code given by PHITS
#4         x0 = [xi pos]                 --> x position on surface of sphere with radius R
#5         y0 = [y0 pos]                 --> y position on surface of sphere with radius R
#6         z0 = [zi pos]                 --> z position on surface of sphere with radius R
#7         z1 = [zi pos]                 --> setting z1 = z0 sets the source to be a point source
#8        dir = all                      --> isotropic particle emission 
#9         e0 = [energy of particle]     --> energy of the particle
#10       wgt = [wgt]                    --> weight of the particle emissions, will change depending on which layer
#                                           currently on (20cm = 3.0 | 30cm = 25.0 | 40cm = 1000.0)


so = "<source> = 1 \n"
st = "s-type = 1 \n"
proj = "proj = "
x0 = "x0 = "
y0 = "y0 = " 
z0 = "z0 = "
z1 = "z1 = "
di = "dir = all \n"
e0 = "e0 = "
wg = "wgt = 3 \n"      #change for each depth


with open("ptesting.dat", 'w+') as file: 
    for num in range(0, N):
        p = proj + str(int(kf[num]))   #convert float to int in order to get of decimals 
        xp = x0 + str(xi[num])
        yp = y0 + str(yi[num])
        zp = z0 + str(zi[num])
        ze = z1 + str(zi[num])
        eg = e0 + str(en[num])
        file.write(so)                  #1
        file.write(st)                  #2
        file.write(p)                   #3
        file.write("\n")
        file.write(xp)                  #4
        file.write("\n")
        file.write(yp)                  #5
        file.write("\n")
        file.write(zp)                  #6
        file.write("\n")
        file.write(ze)                  #7
        file.write("\n")
        file.write(di)                  #8
        file.write(eg)                  #9
        file.write("\n")
        file.write(wg)                  #10
        file.write("\n")
