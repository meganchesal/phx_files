#!/bin/bash

depth=( 5cm 10cm 15cm 20cm 25cm 30cm 35cm 40cm 45cm 50cm 55cm 60cm 65cm 70cm 75cm 80cm 85cm 90cm 95cm 100cm )
material=( PVC )

for j in "${material[@]}"
do
	cd 1GeV_Fe26_$j
        for i in "${depth[@]}"
        do
		mkdir $i
		scp mchesal@smic.hpc.lsu.edu:/work/mchesal/1GeV_Fe26_$j/$i-$j/$i-$j.tar.gz ~/phalanx/git_repository/DATA/1GeV_Fe26_$j/$i/
	done 
	cd ..
done 

exit 0

