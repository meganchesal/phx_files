#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int func(string ifilename) {
	ifstream filein; ofstream fileout;
	string ofilename; string ofilename2;
	
	// change the value to find to be 't', and then get the rest of the values, so you 
	// get the number and the .out
	filein.open(ifilename.c_str());
	if (!filein || !fileout) {
		cout << "Error opening files!" << endl;
		return 1;
	}
	std::size_t found = ifilename.find('t');
	if (found != std::string::npos) {
		string num = ifilename.substr(found + 1);
		//string s(1, num); 
		string file = "edit.out";
		string file2 = "depositfinal";
		ofilename = file;
		ofilename2 = file2 + num;
		cout << "name of output files are: " << ofilename << " and " << ofilename2 << endl;
	}

	fileout.open(ofilename);

	//make new text file that only includes the data, 
	string line; int count = 0; 
	while (getline(filein, line)) {
		count++; 
		if (count > 23) {
			fileout << line << endl;
		}
	}
	filein.close();
	fileout.close();

	//output the data to new file that puts data into a column 
	filein.open(ofilename);
	fileout.open(ofilename2);
	int ncount = 0; int x = 262144;
	while (getline(filein, line, ' ')) {
		ncount++;
		fileout << line << endl;
	}

	filein.close(); fileout.close();

	//check the number of lines outputted to confirm data output is correct
	cout << "512 x 512 is = " << x << " and doubled is =  " << x * 2 << endl;
	cout << "data rows in new file = " << ncount << endl << endl;

	//delete the extra file
	if (remove("edit.out") != 0)
		perror("Error deleting file");
	else
		puts("File sucessfully deleted");


	return 0;
}

int main() {
	
	for (int i = 0; i < 30; i++) {
		int num = i; string s; 
		stringstream ss;
		ss << num; 
		ss >> s; 
		string file = "deposit"; string ext = ".out";
		string filename = file + s + ext; 
		func(filename); 

	}


	return 0;
}