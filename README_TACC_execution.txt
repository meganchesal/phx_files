README: File structure and running things on TACC\

Using many shell scripts, by themselves, and with the batch file to make management and movement of the files easier. Breakdown of how things will be organized, what the shell scripts do, and where the programs should be run (this is specific to Stampede2, will make a different README for file management on SuperMIC) \

WORDS TO NOTE:  Trying to transition running all the batch files in the $SRATCH directory,  The $WORK directory is shared by all users and high I/O code can cause problems.  The $SCRATH directory is subject to purges, but all the important files will be moved back to the $WORK directory in the batch script.
The $SCRATCH directory can be reached with 'cd $SCRATCH', 'cds', or 'cd /scratch/userid/username\' (i.e. ' cd /scratch/07091/mchesal\')
UPDATE:  $SCRATCH directory not working, will continue to run things from $WORK directory, but only a few batches at a time (probably an issue with path names and where stampede2 looks for files)


SHELL SCRIPTS AND THEIR PURPOSES\
1.  Filename --> make_directories1_TACC.sh
	- this file should be run in $WORK in a folder based on what initial particle and its energy (1GeV_Fe)\
	- this file has two arrays, one of various materials, and one with various depths. Depending on which materials you are going to run, change the names and number of materials in the list (do not change the spacing between the parentheses and the equal sign. Must be kept like that).  The same idea if you are only running a couple depths. \
	- based on those two arrays, the program will make a series of folders and subfolders based on those names. This saves trouble from having to hand input all of those files\
	- the result will be a series of folders named by material with subfolders based on depth (name structure for subfolders 10cmmat1) \
	- these will be the folders where the batch files are submitted from.
	- make executable :  chmod a+x make_directories1_TACC.sh

THIS SHELL SCRIPT NOT RELEVANT ANYMORE: EVERYTHING EXECUTED FROM THE WORK DIRECTORY, NOT MOVING ANY FILES 
2. Filename --> make_directories2_TACC.sh
	- this files should be executed in the $WORK directory in folder based on the initial particle and its energy (1GeV_Fe)\
	- this file has two arrays, one of various materials, and one with various depths. Depending on which materials you are going to run, change the names and number of materials in the list (do not change the spacing between the parentheses and the equal sign. Must be kept like that).  The same idea if you are only running a couple depths. \
	- this file makes a series of folders based on the depths and materials used (name structure 10cmmat1).  Unlike the previous file.sh, this code does not create any subfolders, all the folders are together. \
	- these are the folders which will eventually hold the data.  The batch file will move the data into these folders based on their file name. \
	- make executable : chmod a+x make_directories2_TACC.sh

3. Filename --> data_processing.sh
	- this should be in the folder with the batch script and all the input files (i.e. in the $WORK directory in the appropriate subfolders). \
	- this is an additional script that is executed in the batch script.  Underneath the command for the phits file, add in this line \
			./data_processing.sh
	- this will run this shell script after the phits file is done\
	- this file does the additional post-processing needed before running in ROOT\
		= concatenates all the files\
		= replaces the 'D' with 'e'
		= runs the script through the LETBlock_Parse script (names have been made very generic in order to avoid having to compile the .c file each time) \
		= changes the name of the output files based on the folder that it is in (important that you have the right files in the right folders)\
		= puts all of the important output files into a tar ball (.tar.gz) \
	- make executable : chmod a+x data_processing.sh


FILES THAT SHOULD BE IN THE FOLDER YOU SUBMIT FROM: \
1. THE BATCH FILE - filename.knl.mpi.slurm \
-----------------------------------------------------------------------------
#!/bin/bash

#SBATCH -J 22cmPoly		     # Job name
#SBATCH -e errors.e%j      # Name of stderr error file
#SBATCH -p normal          # Queue (partition) name
#SBATCH -N 8               # Total # of nodes
#SBATCH -n 241             # Total # of mpi tasks
#SBATCH -t 0:20:00         # Run time (hh:mm:ss)
#SBATCH --mail-user=megan@spartanphysics.com
#SBATCH --mail-type=all    # Send email at begin and end of job

# Other commands must follow all #SBATCH directives...

module list
pwd
date

# Use ibrun instead of mpirun or mpiexec
ibrun /work/07091/mchesal/stampede2/phits/phits_LinIfort_MPI

./data_processing.sh
-----------------------------------------------------------------------------
 - the number of mpi tasks should be one more than the number of batches listed in the phits input file;  this keeps the number of tasks on each processor to 1, everything runs in one go, rather than a couple processors having to do 2 tasks (shortens the amount of time) \
		= want to keep the number of histories in the input file at around 100,000 (maxis * maxbch = total histories --> 208*480 = 99,840 histories)\

2. DATA_PROCESSING.SH \
 - see above section \

3. PHITS INPUT FILE \
 - this will contain the actual phits information.  The location of the data, and where the files should be outputted and their names needed to be correct and/or adjusted for each of the cases.
 - Things to change\
	= different phits installation location:  file(7) --> nuclear data input file name
	= change the depth?\
			--> file(6) file name of output summary \
			--> [ S u r f a c e ] : change the depth of the material and the distance between the block and target (+placement of the micron of water)\
			--> [ T - C r o s s ] : change the title and the name of the file to match the depth \
	= change the material?\
			--> file(6) file name of output summary \
			--> [ C e l l ] : change the material used and the density (all listed above)\
			--> [ T - C r o s s ] : change the title and the name of the file to match the material \
 - name of the file should include the material and depth (and if the particle and energy changes, that as well) \

4. PHITS.IN FILE \
 - this will change for each case.  This is a text document that states the name of the phits input file and tells 'phits_linIfort_MPI' where to look \

5. LETBLOCK_PARSE.C CODE\
 - this should be the already compiled code.  The version that should be used will have 11 different inputs and has generic input and output names.  This way, the code doesn't have to be recompiled for each case with updated names. \
 - compile the code on the TACC clusters with ' icc ProgramName.c -o ProgramName ' --> ' icc LETBlock_Parse.c -o LETBlock_Parse '
