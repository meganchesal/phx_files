#!/bin/bash

#for the LSU SuperMIC cluster
#concatenating all the files together and doing the character replacment
cat *dmp.dat.* >> cat_file_output.dat
sed -in 's/D/e/gpw output.out' cat_file_output.dat

#running the script to get the LET out
./LETBlock_Parse

#changing the name of the files to match the current directory
mv output_PARSED.dat ${PWD##*/}_PARSED.dat
mv output_pc.out ${PWD##*/}_pc.out
mv output.out ${PWD##*/}_output.dat

tar -czf ${PWD##*/}.tar.gz ${PWD##*/}_PARSED.dat ${PWD##*/}_pc.out ${PWD##*/}_output.dat ${PWD##*/}_summary.out batch errors
